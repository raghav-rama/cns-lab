import java.util.Scanner;

class AES {

    private static final String KEY = "1234567890123456"; // 128-bit key

    public static byte[] encrypt(String strToEncrypt, String key) {
        byte[] keyBytes = key.getBytes();
        byte[] dataBytes = strToEncrypt.getBytes();
        int blockLength = 16;
        int paddedLength = blockLength * ((dataBytes.length + blockLength - 1) / blockLength);
        byte[] result = new byte[paddedLength];

        for (int i = 0; i < paddedLength; i++) {
            result[i] = (byte) (dataBytes[i % dataBytes.length] ^ keyBytes[i % blockLength]);
        }
        return result;
    }

    public static String decrypt(byte[] encryptedBytes, String key) {
        byte[] keyBytes = key.getBytes();
        byte[] decryptedData = new byte[encryptedBytes.length];

        for (int i = 0; i < encryptedBytes.length; i++) {
            decryptedData[i] = (byte) (encryptedBytes[i] ^ keyBytes[i % keyBytes.length]);
        }
        return new String(decryptedData).trim();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the plaintext: ");
        String originalString = scanner.nextLine();

        try {
            byte[] encryptedBytes = encrypt(originalString, KEY);
            StringBuilder encryptedStringBuilder = new StringBuilder();
            for (byte encryptedByte : encryptedBytes) {
                encryptedStringBuilder.append(String.format("%02x", encryptedByte));
            }
            String encryptedString = encryptedStringBuilder.toString();

            byte[] encryptedBytesArray = new byte[encryptedString.length() / 2];
            for (int i = 0; i < encryptedString.length(); i += 2) {
                encryptedBytesArray[i / 2] = (byte) ((Character.digit(encryptedString.charAt(i), 16) << 4)
                        + Character.digit(encryptedString.charAt(i + 1), 16));
            }
            String decryptedString = decrypt(encryptedBytesArray, KEY);

            System.out.println("Original String: " + originalString);
            System.out.println("Encrypted String: " + encryptedString);
            System.out.println("Decrypted String: " + decryptedString);
        } catch (Exception e) {
            System.out.println("Encryption/Decryption error: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }
}