import java.util.Scanner;

class AES {

    private static final String KEY = "1234567890123456"; // 128-bit key

    public static byte[] encrypt(String strToEncrypt, String key) {
        byte[] keyBytes = key.getBytes();
        byte[] dataBytes = strToEncrypt.getBytes();
        int blockLength = 16;
        int paddedLength = blockLength * ((dataBytes.length + blockLength - 1) / blockLength);
        byte[] result = new byte[paddedLength];

        for (int i = 0; i < paddedLength; i++) {
            result[i] = (byte) (dataBytes[i % dataBytes.length] ^ keyBytes[i % blockLength]);
        }
        return result;
    }

    public static String decrypt(byte[] encryptedBytes, String key) {
        byte[] keyBytes = key.getBytes();
        byte[] decryptedData = new byte[encryptedBytes.length];

        for (int i = 0; i < encryptedBytes.length; i++) {
            decryptedData[i] = (byte) (encryptedBytes[i] ^ keyBytes[i % keyBytes.length]);
        }
        return new String(decryptedData).trim();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the plaintext: ");
        String originalString = scanner.nextLine();

        try {
            byte[] encryptedBytes = encrypt(originalString, KEY);
            StringBuilder encryptedStringBuilder = new StringBuilder();
            for (byte encryptedByte : encryptedBytes) {
                encryptedStringBuilder.append(String.format("%02x", encryptedByte));
            }
            String encryptedString = encryptedStringBuilder.toString();

            byte[] encryptedBytesArray = new byte[encryptedString.length() / 2];
            for (int i = 0; i < encryptedString.length(); i += 2) {
                encryptedBytesArray[i / 2] = (byte) ((Character.digit(encryptedString.charAt(i), 16) << 4)
                        + Character.digit(encryptedString.charAt(i + 1), 16));
            }
            String decryptedString = decrypt(encryptedBytesArray, KEY);

            System.out.println("Original String: " + originalString);
            System.out.println("Encrypted String: " + encryptedString);
            System.out.println("Decrypted String: " + decryptedString);
        } catch (Exception e) {
            System.out.println("Encryption/Decryption error: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }

}
##########################################################################################################################################################################
import java.util.Scanner;

public class CaesarCipher {
public static void main(String[] args) {
Scanner scanner = new Scanner(System.in);

        System.out.println("Enter any String:");
        String str = scanner.nextLine();

        System.out.println("Enter the key:");
        int key = scanner.nextInt();

        String encryptedString = processString(str, key, true);
        System.out.println("\nEncrypted string is: " + encryptedString);

        String decryptedString = processString(encryptedString, key, false);
        System.out.println("\nDecrypted string is: " + decryptedString);

        scanner.close();
    }

    public static String processString(String str, int key, boolean encrypt) {
        StringBuilder result = new StringBuilder();
        int direction = encrypt ? 1 : -1;

        for (char ch : str.toCharArray()) {
            if (Character.isUpperCase(ch)) {
                ch = (char) ('A' + (ch - 'A' + direction * key + 26) % 26);
            } else if (Character.isLowerCase(ch)) {
                ch = (char) ('a' + (ch - 'a' + direction * key + 26) % 26);
            }
            result.append(ch);
        }
        return result.toString();
    }

}

##########################################################################################################################################################################
#include<stdio.h>
// Initial permutation (IP) table
int IP[] = {2, 6, 3, 1, 4, 8, 5, 7};
// Inverse initial permutation (IP^-1) table
int IP_inverse[] = {4, 1, 3, 5, 7, 2, 8, 6};
// S-boxes
int S0[4][4] = {
{1, 0, 3, 2},
{3, 2, 1, 0},
{0, 2, 1, 3},
{3, 1, 3, 2}
};
int S1[4][4] = {
{0, 1, 2, 3},
{2, 0, 1, 3},
{3, 0, 1, 0},
{2, 1, 0, 3}
};
// Function to perform initial permutation
int ip(int plaintext) {
int result = 0;
for (int i = 0; i < 8; i++) {
result |= ((plaintext >> (8 - IP[i])) & 1) << (7 - i);
}
return result;
}
// Function to perform inverse initial permutation
int inverse_ip(int ciphertext) {
int result = 0;
for (int i = 0; i < 8; i++) {
result |= ((ciphertext >> (8 - IP_inverse[i])) & 1) << (7 - i);
}
return result;
}
// Function to perform S-box substitution
int s_box_substitution(int value, int s_box[4][4]) {
int row = ((value & 0b1000) >> 2) | (value & 0b0001);
int col = (value & 0b0110) >> 1;
return s_box[row][col];
}
int main() {
int plaintext = 0b11010110;
printf("Plain Text: %x\n", plaintext);
int ciphertext = ip(plaintext);
printf("Cipher Text: %x\n", ciphertext);

    // Example of S-box substitution
    int val = 0b1101; // Example value
    int sboxres = s_box_substitution(val, S0);
    printf("S-box result: %x\n", sboxres);

    int decrypted_text = inverse_ip(ciphertext);
    printf("Decrypted Text: %x\n", decrypted_text);
    return 0;

}

##########################################################################################################################################################################

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Diffie-Hellman Key Exchange</title>
  </head>
  <body>
    <h1>Diffie-Hellman Key Exchange</h1>
    <label for="prime">Prime Number (p):</label>
    <input type="number" id="prime" value="23" /><br /><br />
    <label for="base">Base Number (g):</label>
    <input type="number" id="base" value="5" /><br /><br />
    <label for="alicePrivateKey">Alice's Private Key (a):</label>
    <input type="number" id="alicePrivateKey" /><br /><br />
    <label for="bobPrivateKey">Bob's Private Key (b):</label>
    <input type="number" id="bobPrivateKey" /><br /><br />
    <button onclick="calculateKeys()">Calculate Keys</button>

    <h2>Results</h2>
    <p>Alice's Public Key (A): <span id="alicePublicKey"></span></p>
    <p>Bob's Public Key (B): <span id="bobPublicKey"></span></p>
    <p>Shared Secret Key (s): <span id="sharedKey"></span></p>

    <script>
      function modExp(base, exp, mod) {
        return Math.pow(base, exp) % mod;
      }

      function calculateKeys() {
        const p = parseInt(document.getElementById("prime").value);
        const g = parseInt(document.getElementById("base").value);
        const a = parseInt(document.getElementById("alicePrivateKey").value);
        const b = parseInt(document.getElementById("bobPrivateKey").value);

        const A = modExp(g, a, p);
        const B = modExp(g, b, p);

        const sharedKeyAlice = modExp(B, a, p);
        const sharedKeyBob = modExp(A, b, p);

        document.getElementById("alicePublicKey").textContent = A;
        document.getElementById("bobPublicKey").textContent = B;
        document.getElementById("sharedKey").textContent = sharedKeyAlice;

        if (sharedKeyAlice !== sharedKeyBob) {
          console.error("Shared keys do not match!");
        }
      }
    </script>

  </body>
</html>

##########################################################################################################################################################################
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LEN 100

void encrypt(char \*message, int key[][MAX_LEN], int n)
{
int len = strlen(message);
int i, j, k;
int cipher[MAX_LEN] = {0};

    for (i = 0; i < len; i += n)
    {
        for (j = 0; j < n; j++)
        {
            int sum = 0;
            for (k = 0; k < n; k++)
            {
                sum += key[j][k] * (message[i + k] - 'A');
            }
            cipher[i + j] = sum % 26;
        }
    }

    for (i = 0; i < len; i++)
    {
        message[i] = cipher[i] + 'A';
    }

}

void decrypt(char \*message, int key[][MAX_LEN], int n)
{
int len = strlen(message);
int i, j, k;
int decipher[MAX_LEN] = {0};

    int det = key[0][0] * (key[1][1] * key[2][2] - key[1][2] * key[2][1]) -
              key[0][1] * (key[1][0] * key[2][2] - key[1][2] * key[2][0]) +
              key[0][2] * (key[1][0] * key[2][1] - key[1][1] * key[2][0]);

    int detInverse = 0;
    for (detInverse = 1; detInverse < 26; detInverse++)
    {
        if ((det * detInverse) % 26 == 1)
            break;
    }

    int adj[MAX_LEN][MAX_LEN];
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            adj[j][i] = ((key[(i + 1) % n][(j + 1) % n] * key[(i + 2) % n][(j + 2) % n]) -
                         (key[(i + 1) % n][(j + 2) % n] * key[(i + 2) % n][(j + 1) % n]));

            adj[j][i] = (adj[j][i] + 26) % 26;
        }
    }

    int invKey[MAX_LEN][MAX_LEN];
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            invKey[i][j] = (adj[i][j] * detInverse) % 26;

            if (invKey[i][j] < 0)
                invKey[i][j] += 26;
        }
    }

    for (i = 0; i < len; i += n)
    {
        for (j = 0; j < n; j++)
        {
            int sum = 0;
            for (k = 0; k < n; k++)
            {
                sum += invKey[j][k] * (message[i + k] - 'A');
            }
            decipher[i + j] = sum % 26;

            if (decipher[i + j] < 0)
                decipher[i + j] += 26;
        }
    }

    for (i = 0; i < len; i++)
    {
        message[i] = decipher[i] + 'A';
    }

}

int main()
{
char message[MAX_LEN];
int n, i, j;

    printf("Enter the dimension of the key matrix: ");
    scanf("%d", &n);

    int key[MAX_LEN][MAX_LEN];
    printf("Enter the key matrix:\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            scanf("%d", &key[i][j]);
        }
    }

    printf("Enter the message for encryption:\n");
    scanf("%s", message);

    printf("\n");
    encrypt(message, key, n);

    printf("Encrypted message: %s\n", message);

    decrypt(message, key, n);
    printf("Decrypted message: %s\n", message);

    return 0;

}
##########################################################################################################################################################################
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {
public static String getMd5(String input) {
try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            // of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    // Driver code
    public static void main(String args[]) throws NoSuchAlgorithmException {
        String s = "Shamrila";
        System.out.println("Your HashCode Generated by MD5 is: " + getMd5(s));
    }

}
##########################################################################################################################################################################
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class PlayfairCipher {
private char[][] matrix = new char[5][5];

    public PlayfairCipher(String key) {
        Set<Character> charset = new LinkedHashSet<>();
        for (char c : (key.toUpperCase() + "ABCDEFGHIKLMNOPQRSTUVWXYZ").toCharArray()) {
            if (c != 'J') charset.add(c);
        }
        int i = 0;
        for (char c : charset) matrix[i / 5][i++ % 5] = c;
    }

    private int[] findPosition(char c) {
        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 5; j++)
                if (matrix[i][j] == c) return new int[]{i, j};
        return null;
    }

    private String processText(String text, boolean encrypt) {
        StringBuilder result = new StringBuilder();
        text = text.toUpperCase().replaceAll("[^A-Z]", "").replace('J', 'I');
        if (text.length() % 2 != 0) text += 'X';
        for (int i = 0; i < text.length(); i += 2) {
            char a = text.charAt(i), b = text.charAt(i + 1);
            if (a == b) b = 'X';
            int[] posA = findPosition(a), posB = findPosition(b);
            if (posA[0] == posB[0] || posA[1] == posB[1]) {
                result.append(matrix[posA[0]][(posA[1] + (encrypt ? 1 : 4)) % 5]);
                result.append(matrix[posB[0]][(posB[1] + (encrypt ? 1 : 4)) % 5]);
            } else {
                result.append(matrix[posA[0]][posB[1]]);
                result.append(matrix[posB[0]][posA[1]]);
            }
        }
        return result.toString();
    }

    public String encrypt(String plaintext) {
        return processText(plaintext, true);
    }

    public String decrypt(String ciphertext) {
        return processText(ciphertext, false);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the key:");
        String key = scanner.nextLine();
        System.out.println("Enter the plaintext:");
        String plaintext = scanner.nextLine();
        PlayfairCipher cipher = new PlayfairCipher(key);
        String ciphertext = cipher.encrypt(plaintext);
        System.out.println("Encrypted text: " + ciphertext);
        System.out.println("Decrypted text: " + cipher.decrypt(ciphertext));
        scanner.close();
    }

}

##########################################################################################################################################################################
#include <stdio.h>
int main()
{
char str[12];
printf("Enter a string: ");
scanf("%[^\n]%\*c", str);

int length = 0;
while (str[length] != '\0')
length++;

char strAnd[length + 1];
char strXor[length + 1];

for (int i = 0; i < length; i++)
{
strAnd[i] = str[i] & 127;
strXor[i] = str[i] ^ 127;
}
strAnd[length] = '\0';
strXor[length] = '\0';

printf("The input string is: %s\n", str);
printf("The AND result: %s\n", strAnd);
printf("The XOR result: %s\n", strXor);

return 0;
}

##########################################################################################################################################################################
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
void main()
{
int i,j,len,rails,count,code[100][1000];
char str[1000];
printf("Enter a Secret Message\n");
scanf("%[^\n]",str);
len=strlen(str);
printf("Enter number of rails\n");
scanf("%d",&rails);
printf("\nEncrypted text:\n");
for(i=0;i<rails;i++)
{
for(j=0;j<len;j++)
{
code[i][j]=0;
}
}
count=0;
j=0;
while(j<len)
{
if(count%2==0)
{
for(i=0;i<rails;i++)
{
//strcpy(code[i][j],str[j]);
code[i][j]=(int)str[j];
j++;
}

}
else
{

for(i=rails-2;i>0;i--)
{
code[i][j]=(int)str[j];
j++;
}  
}

count++;
}
for(i=0;i<rails;i++)
{
for(j=0;j<len;j++)
{
if(code[i][j]!=0)
printf("%c",code[i][j]);
}
}
printf("\n");
}
##########################################################################################################################################################################
import java.math.BigInteger;
import java.security.SecureRandom;

public class RSA {
private final static BigInteger ONE = BigInteger.ONE;
private final static SecureRandom random = new SecureRandom();

    private BigInteger privateKey;
    private BigInteger publicKey;
    private BigInteger modulus;

    public RSA(int bits) {
        BigInteger p = BigInteger.probablePrime(bits / 2, random);
        BigInteger q = BigInteger.probablePrime(bits / 2, random);
        modulus = p.multiply(q);
        BigInteger phi = (p.subtract(ONE)).multiply(q.subtract(ONE));
        publicKey = new BigInteger("65537");
        privateKey = publicKey.modInverse(phi);
    }

    public BigInteger encrypt(BigInteger message) {
        return message.modPow(publicKey, modulus);
    }

    public BigInteger decrypt(BigInteger encrypted) {
        return encrypted.modPow(privateKey, modulus);
    }

    public static void main(String[] args) {
        RSA rsa = new RSA(1024);

        String plaintext = "Hello, RSA!";
        BigInteger message = new BigInteger(plaintext.getBytes());

        BigInteger encrypted = rsa.encrypt(message);
        BigInteger decrypted = rsa.decrypt(encrypted);

        String decryptedText = new String(decrypted.toByteArray());

        System.out.println("Plaintext: " + plaintext);
        System.out.println("Encrypted: " + encrypted);
        System.out.println("Decrypted: " + decryptedText);
    }

}

##########################################################################################################################################################################

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA512 {
public static String encryptThisString(String input) {
try {
// getInstance() method is called with algorithm SHA-512
MessageDigest md = MessageDigest.getInstance("SHA-512");

            // digest() method is called
            // to calculate message digest of the input string
            // returned as array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);

            // Add preceding 0s to make it 32 bit
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

            // return the HashText
            return hashtext;
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    // Driver code
    public static void main(String args[]) throws NoSuchAlgorithmException {
        System.out.println("HashCode Generated by SHA-512 for: ");

        String s1 = "Sharmila";
        System.out.println("\n" + s1 + " : " + encryptThisString(s1));

        String s2 = "hello sharmila";
        System.out.println("\n" + s2 + " : " + encryptThisString(s2));
    }

}

##########################################################################################################################################################################
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

void printVigenereTable() {
printf("Vigenère Table:\n");
printf(" A B C D E F G H I J K L M N O P Q R S T U V W X Y Z\n");
for (int i = 0; i < 26; i++) {
printf("%c ", 'A' + i);
for (int j = 0; j < 26; j++) {
printf("%c ", 'A' + (i + j) % 26);
}
printf("\n");
}
}

void encrypt() {
char plaintext[128];
char key[16];
printf("\nEnter the plaintext (up to 128 characters): ");
scanf(" %[^\n]", plaintext);
printf("Enter the key (up to 16 characters): ");
scanf(" %[^\n]", key);

    printf("Cipher Text: ");
    for (int i = 0, j = 0; i < strlen(plaintext); i++, j++) {
        if (j >= strlen(key)) {
            j = 0;
        }
        int shift = toupper(key[j]) - 'A';
        char encryptedChar = ((toupper(plaintext[i]) - 'A' + shift) % 26) + 'A';
        printf("%c", encryptedChar);
    }
    printf("\n");

}

void decrypt() {
char ciphertext[128];
char key[16];
printf("\nEnter the ciphertext: ");
scanf(" %[^\n]", ciphertext);
printf("Enter the key: ");
scanf(" %[^\n]", key);

    printf("Deciphered Text: ");
    for (int i = 0, j = 0; i < strlen(ciphertext); i++, j++) {
        if (j >= strlen(key)) {
            j = 0;
        }
        int shift = toupper(key[j]) - 'A';
        char decryptedChar = ((toupper(ciphertext[i]) - 'A' - shift + 26) % 26) + 'A';
        printf("%c", decryptedChar);
    }
    printf("\n");

}

int main() {
int option;
while (1) {
printf("\n1. Encrypt");
printf("\n2. Decrypt");
printf("\n3. Print Vigenère Table");
printf("\n4. Exit\n");
printf("\nEnter your option: ");
scanf("%d", &option);

        switch (option) {
            case 1:
                encrypt();
                break;
            case 2:
                decrypt();
                break;
            case 3:
                printVigenereTable();
                break;
            case 4:
                exit(0);
            default:
                printf("\nInvalid selection! Try again.\n");
                break;
        }
    }
    return 0;

}
